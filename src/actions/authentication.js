import axios from 'axios'

import {
    AUTH_AUTHENTICATED,
    AUTH_UNAUTHENTICATED,
    AUTH_AUTHENTICATION_ERROR
} from '../utils'
import {
    API_AUTH_URL, API_URL
} from '../utils/urls'

export function authLogin ({ username, password }, history) {
    return (dispatch) => {
        const data = {
            username,
            password
        }
        return axios.post(`${API_URL}${API_AUTH_URL}`, data).then(response => {
            dispatch({
                type: AUTH_AUTHENTICATED,
                payload: response.data
            })
            history.push('/')
        }).catch(error => {
            dispatch({
                type: AUTH_AUTHENTICATION_ERROR,
                payload: 'Invalid username or password'
            })
        })
    }
}

export function authLogout () {
    return (dispatch) => {
        dispatch({
            type: AUTH_UNAUTHENTICATED
        })
    }
}
