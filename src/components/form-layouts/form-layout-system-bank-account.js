import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import client from '../../utils/client'
import { API_BANK_URL, API_CUSTOMER_LEVEL_URL } from '../../utils/urls'
import InputField from '../forms/form-input'
import SelectField from '../forms/form-select'
import { Button } from 'reactstrap'
import { numericality, required, length } from 'redux-form-validators'
import { getBankIcon } from '../../utils/helpers'

class FormSystemBankAccount extends Component {
    constructor (props) {
        super(props)
        this.state = {
            banks: [],
            customerLevels: []
        }
    }

    componentDidMount () {
        this.fetchBanks()
        this.fetchCustomerLevels()
    }

    fetchBanks = () => {
        client.get(API_BANK_URL).then(res => {
            this.setState({
                banks: res.data
            })
        })
    }

    fetchCustomerLevels = () => {
        client.get(API_CUSTOMER_LEVEL_URL).then(res => {
            this.setState({
                customerLevels: res.data
            })
        })
    }

    handleSubmit = (value) => {
        this.props.onFormSubmit(value, this.props.reset)
    }

    getOptionValue = option => option.id
    getOptionLabel = option => option.name
    getOptionBankLabel = option => <div>{getBankIcon(option.code)} {option.name}</div>

    render () {
        const {
            reset,
            pristine,
            submitting,
            handleSubmit
        } = this.props
        const {
            banks,
            customerLevels
        } = this.state
        return (
            <form onSubmit={handleSubmit(this.handleSubmit)}>
                <Field component={SelectField} label='ธนาคาร' name='bank' validate={required()} options={banks} getOptionLabel={this.getOptionBankLabel} getOptionValue={this.getOptionValue} />
                <Field component={InputField} label='ชื่อบัญชี' name='name' validate={required()} />
                <Field component={InputField} label='เลขบัญชี' name='account_no' validate={required()} />
                <Field component={InputField} type='number' label='จำนวนเงิน' name='balance' validate={[required(), numericality({ '>': 0 })]} />
                <Field component={SelectField} label='ระดับสมาชิก' name='customer_levels' validate={[required(), length({ minimum: 1 })]} options={customerLevels} getOptionLabel={this.getOptionLabel} getOptionValue={this.getOptionValue} isMulti isClearable />
                <Button type='submit' color='info' disabled={pristine || submitting}>บันทึก</Button>
                <Button color='link' onClick={reset}>ล้าง</Button>
            </form>
        )
    }
}

export default reduxForm({
    form: 'systemAccountBank',
    enableReinitialize: true
})(FormSystemBankAccount)
