import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, Switch, withRouter } from 'react-router-dom'
import Sidebar from '../sidebar'
import TopBarHeader from '../topbar-header'
import Error404View from '../../views/error404'
import PermissionRoute from '../mixins/permission-route'
import routes from '../../utils/routes'

class LayoutAuthenticated extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            isShowSideBar: false
        }
    }

    onClickShowSideBar = () => {
        this.setState({
            isShowSideBar: !this.state.isShowSideBar
        })
    }

    getRoute = (route) => {
        const routePath = route.path
        if (route.isRedirectRoute) {
            return <Redirect key={routePath} exact from={routePath} to={route.to} />
        } else if (route.requiredPermission) {
            return <PermissionRoute key={routePath} exact path={routePath} component={route.component} requiredPermission={route.requiredPermission} />
        }
        return <Route key={routePath} exact path={routePath} component={route.component} />
    }

    getRoutes = () => {
        let routeList = []
        routes.map(route => {
            const subRoutes = route.subRoutes || []
            if (subRoutes.length > 0) {
                subRoutes.map(subRoute => { routeList.push(this.getRoute(subRoute)) })
            } else {
                routeList.push(this.getRoute(route))
            }
        })
        return routeList
    }

    render () {
        const { isShowSideBar } = this.state
        return (
            <div id='main-wrapper' className={`${isShowSideBar ? 'show-sidebar' : ''}`}>
                <TopBarHeader isShowSideBar={isShowSideBar} onClickShowSideBar={this.onClickShowSideBar} />
                <Sidebar />

                <Switch>
                    {this.getRoutes()}
                    <Route component={Error404View} />
                </Switch>
            </div>
        )
    }
}

export default withRouter(LayoutAuthenticated)
