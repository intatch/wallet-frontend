import React, { Component } from 'react'
import { Field, reduxForm } from 'redux-form'
import { withRouter } from 'react-router-dom'
import Swal from 'sweetalert2'
import client from '../../utils/client'
import {
    API_USER_CHANGE_PASSWORD_URL
} from '../../utils/urls'
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import InputField from '../../components/forms/form-input'
import { required, confirmation } from 'redux-form-validators'

class modalChangePassword extends Component {
    toggle = () => {
        this.props.toggle()
    }

    onClosed = () => {
        this.props.reset()
    }

    handleSubmit = (value) => {
        const {
            userId
        } = this.props
        client.post(API_USER_CHANGE_PASSWORD_URL.replace('userId', userId), value).then(res => {
            Swal.fire({
                type: 'success',
                title: 'เปลี่ยนรหัสผ่านสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            this.toggle()
        })
    }

    render () {
        const {
            pristine,
            submitting,
            handleSubmit,
            isModalOpen
        } = this.props

        return (
            <Modal isOpen={isModalOpen} toggle={this.toggle} onClosed={this.onClosed}>
                <ModalHeader>เปลี่ยนรหัสผ่าน</ModalHeader>
                <form onSubmit={handleSubmit(this.handleSubmit)}>
                    <ModalBody>
                        <Field component={InputField} type='password' label='รหัสผ่าน' name='password' validate={required()} />
                        <Field component={InputField} type='password' label='ยืนยันรหัสผ่าน' name='password_confirmation' validate={confirmation({ field: 'password', fieldLabel: 'รหัสผ่าน' })} />
                    </ModalBody>
                    <ModalFooter>
                        <Button color='link' onClick={this.toggle}>ยกเลิก</Button>
                        <Button type='submit' color='info' className='mr-1' disabled={pristine || submitting}>บันทึก</Button>
                    </ModalFooter>
                </form>
            </Modal>
        )
    }
}

const reduxFormModalChangePassword = reduxForm({
    form: 'modalChangePassword',
    enableReinitialize: true
})(modalChangePassword)

export default withRouter(reduxFormModalChangePassword)
