import React, { Component } from 'react'
import { Modal, ModalBody, ModalHeader } from 'reactstrap'
import { getTransactionState, getDateFormat } from '../../utils/helpers'
import LayoutTransactionHistory from '../layouts/layout-transaction-history'

class ModalTransactionCreditDetail extends Component {
    toggle = () => {
        this.props.toggle()
    }

    render () {
        const {
            isModalOpen,
            transactionCredit
        } = this.props

        if (transactionCredit === null) {
            return ''
        }

        return (
            <Modal isOpen={isModalOpen} toggle={this.toggle} size='lg'>
                <ModalHeader toggle={this.toggle}>ธุรกรรมการเครดิตเลขที่ {transactionCredit.id}</ModalHeader>
                <ModalBody>
                    <h3 >รายละเอียดการทำธุรกรรม {getTransactionState(transactionCredit.state)}</h3>
                    <hr />
                    <div className='row'>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>เกม</h5>
                            <h6>{transactionCredit.game.name}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>บัญชีเครดิต</h5>
                            <h6>{transactionCredit.customer}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>จำนวนเงิน</h5>
                            <h6>{transactionCredit.credit}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>โบนัส</h5>
                            <h6>{transactionCredit.bonus_credit}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>รวม</h5>
                            <h6><ins>{transactionCredit.total_credit}</ins></h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>วันเวลาที่ทำรายการ</h5>
                            <h6>{getDateFormat(transactionCredit.created_at)}</h6>
                        </div>
                    </div>
                    <h3 >รายละเอียดลูกค้า</h3>
                    <hr />
                    <div className='row'>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>ชื่อลูกค้า</h5>
                            <h6>{transactionCredit.customer_credit_account_customer.fullname}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>Line ID</h5>
                            <h6>{transactionCredit.customer_credit_account_customer.line_id}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>Email</h5>
                            <h6>{transactionCredit.customer_credit_account_customer.email}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>เบอร์โทรศัพท์</h5>
                            <h6>{transactionCredit.customer_credit_account_customer.telephone}</h6>
                        </div>
                        <div className='col-md-6'>
                            <h5 className='font-weight-bold'>ระดับลูกค้า</h5>
                            <h6>{transactionCredit.customer_credit_account_customer.customer_level.name}</h6>
                        </div>
                    </div>
                    <hr />
                    <LayoutTransactionHistory transactions={transactionCredit.transaction_history} />
                </ModalBody>

            </Modal>
        )
    }
}

export default ModalTransactionCreditDetail
