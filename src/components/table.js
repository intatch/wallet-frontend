import React from 'react'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'

export const getNoDataTableBody = (colSpan) => {
    return (
        <TableRow>
            <TableCell colSpan={colSpan} className='text-center text-danger'>ไม่พบข้อมูล</TableCell>
        </TableRow>
    )
}
