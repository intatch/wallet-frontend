import { AUTH_UNAUTHENTICATED } from './utils'

export function authenticatedMiddleware ({ dispatch }) {
    return (next) => (action) => {
        if (typeof action === 'function') {
            const refreshToken = window.localStorage.getItem('refreshToken')
            if (!refreshToken) {
                dispatch({
                    type: AUTH_UNAUTHENTICATED
                })
            }
        }
        return next(action)
    }
}
