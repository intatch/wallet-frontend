import Push from 'push.js'

export const requestPermission = () => {
    if (Push.Permission.get() === Push.Permission.DEFAULT) {
        Push.Permission.request(() => {
            console.log('permission granted')
        }, () => {
            console.log('permission denied')
        })
    }
}

export const createNotification = (body) => {
    if (Push.Permission.get() === Push.Permission.GRANTED) {
        Push.create('การแจ้งเตือนจากระบบ', {
            body: body,
            timeout: 4000,
            onClick: () => {
                window.focus()
                this.close()
            }
        })
    }
}
