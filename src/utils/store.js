import thunk from 'redux-thunk'
import storage from 'redux-persist/lib/storage'
import { persistReducer, persistStore } from 'redux-persist'
import { composeWithDevTools } from 'redux-devtools-extension'
import { applyMiddleware, createStore } from 'redux'

import rootReducer from '../reducers'
import { authenticatedMiddleware } from '../middlewares'

const persistConfig = {
    key: 'root',
    storage: storage,
    blacklist: ['form', 'message', 'global']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)
const middleware = applyMiddleware(authenticatedMiddleware, thunk)
const devTools = process.env.NODE_ENV === 'production' ? middleware : composeWithDevTools(middleware)

const createStoreWithMiddleware = devTools(createStore)

export const store = createStoreWithMiddleware(persistedReducer)
export const persistor = persistStore(store)
