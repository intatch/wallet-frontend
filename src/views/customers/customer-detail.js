import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_CUSTOMER_CREDIT_ACCOUNT_URL, API_CUSTOMER_DETAIL_URL } from '../../utils/urls'
import client from '../../utils/client'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import { getNoDataTableBody } from '../../components/table'
import { Button, Card, CardBody } from 'reactstrap'
import { getBankIcon } from '../../utils/helpers'
import ModalCustomerCreditAccount from '../../components/modals/modal-customer-credit-account'

class CustomerDetailView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            customer: null,
            selectedCreditAccount: null,
            apiCreditAccountEndpoint: null,
            isModalCreditAccountOpen: false
        }
    }

    componentDidMount () {
        this.fetchCustomer()
    }

    fetchCustomer = () => {
        const {
            match: { params }
        } = this.props
        client.get(API_CUSTOMER_DETAIL_URL.replace('customerId', params.id)).then(res => {
            this.setState({ customer: res.data })
        })
    }

    toggleModalCreditAccount = () => {
        this.setState({
            selectedCreditAccount: null,
            apiCreditAccountEndpoint: API_CUSTOMER_CREDIT_ACCOUNT_URL,
            isModalCreditAccountOpen: !this.state.isModalCreditAccountOpen
        })
    }

    onCreditAccountSubmitCallback = (res) => {
        const {
            customer
        } = this.state
        const creditAccount = res.data
        const customerData = {
            ...customer,
            credit_accounts: [...customer.credit_accounts, creditAccount]
        }
        this.setState({
            customer: customerData,
            isModalCreditAccountOpen: false
        })
    }

    getBankAccountTableBody = () => {
        const {
            customer
        } = this.state
        if (!customer || customer.bank_accounts.length === 0) {
            return getNoDataTableBody(2)
        }
        return customer.bank_accounts.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{getBankIcon(row.bank.code)} {row.bank.name}</TableCell>
                    <TableCell>{row.bank_no}</TableCell>
                </TableRow>
            )
        })
    }

    getCreditAccountTableBody = () => {
        const {
            customer
        } = this.state
        if (!customer || customer.credit_accounts.length === 0) {
            return getNoDataTableBody(3)
        }
        return customer.credit_accounts.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.game.name}</TableCell>
                    <TableCell>{row.username}</TableCell>
                    <TableCell>{row.password}</TableCell>
                </TableRow>
            )
        })
    }

    render () {
        const {
            match: { params }
        } = this.props

        const {
            customer,
            selectedCreditAccount,
            apiCreditAccountEndpoint,
            isModalCreditAccountOpen
        } = this.state
        const customerUsername = customer ? customer.username : '-'
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>รายละเอียด {customerUsername}</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/customers'>ลูกค้า</Link>
                        </li>
                        <li className='breadcrumb-item active'>{customerUsername}</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='row'>
                        <div className='col-md-6'>
                            <Card className='m-4'>
                                <CardBody>
                                    <div className='d-flex align-items-center'>
                                        <h4 className='card-title'>ข้อมูลลูกค้า</h4>
                                        <div className='ml-auto'>
                                            <Link to={`/customers/${params.id}/edit`} className='btn btn-warning'>
                                                <EditIcon /> แก้ไขข้อมูล
                                            </Link>
                                        </div>
                                    </div>
                                    {customer &&
                                        <div className='row'>
                                            <div className='col-md-6'>
                                                <h5 className='font-weight-bold'>Username</h5>
                                                <h6>{customer.username}</h6>
                                            </div>
                                            <div className='col-md-6'>
                                                <h5 className='font-weight-bold'>Email</h5>
                                                <h6>{customer.email}</h6>
                                            </div>
                                            <div className='col-md-6'>
                                                <h5 className='font-weight-bold'>ชื่อลูกค้า</h5>
                                                <h6>{customer.fullname}</h6>
                                            </div>
                                            <div className='col-md-6'>
                                                <h5 className='font-weight-bold'>เบอร์โทรศัพท์</h5>
                                                <h6>{customer.telephone}</h6>
                                            </div>
                                            <div className='col-md-6'>
                                                <h5 className='font-weight-bold'>Line ID</h5>
                                                <h6>{customer.line_id}</h6>
                                            </div>
                                            <div className='col-md-6'>
                                                <h5 className='font-weight-bold'>ระดับลูกค้า</h5>
                                                <h6>{customer.customer_level.name}</h6>
                                            </div>
                                        </div>
                                    }
                                </CardBody>
                            </Card>
                            <Card className='m-4'>
                                <CardBody>
                                    <h4 className='card-title'>ข้อมูลบัญชีธนาคาร</h4>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>ธนาคาร</TableCell>
                                                <TableCell>เลขที่บัญชี</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {this.getBankAccountTableBody()}
                                        </TableBody>
                                    </Table>
                                </CardBody>
                            </Card>
                        </div>

                        <div className='col-md-6'>
                            <Card className='m-4'>
                                <CardBody>
                                    <div className='d-flex align-items-center'>
                                        <h4 className='card-title'>ข้อมูลบัญชีเครดิต</h4>
                                        <div className='ml-auto'>
                                            <Button color='info' onClick={this.toggleModalCreditAccount}>
                                                <AddIcon /> เพิ่มบัญชีเครดิต
                                            </Button>
                                        </div>
                                    </div>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>เกม</TableCell>
                                                <TableCell>Username</TableCell>
                                                <TableCell>Password</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {this.getCreditAccountTableBody()}
                                        </TableBody>
                                    </Table>
                                </CardBody>
                            </Card>
                            <ModalCustomerCreditAccount
                                customerId={params.id}
                                customer={customer}
                                apiEndpoint={apiCreditAccountEndpoint}
                                initialValues={selectedCreditAccount}
                                isModalOpen={isModalCreditAccountOpen}
                                toggle={this.toggleModalCreditAccount}
                                onSubmitCallback={this.onCreditAccountSubmitCallback} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(CustomerDetailView)
