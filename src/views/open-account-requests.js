import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import client from '../utils/client'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { UncontrolledTooltip } from 'reactstrap'
import { getNoDataTableBody } from '../components/table'
import CancelIcon from '@material-ui/icons/Cancel'
import {
    API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL,
    API_CUSTOMER_CREDIT_ACCOUNT_URL
} from '../utils/urls'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import { STATUS_APPROVED, STATUS_REJECTED } from '../utils'
import Swal from 'sweetalert2'
import PusherService from '../utils/pusher'
import _ from 'lodash'

class OpenAccountRequestView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            customerCreditAccounts: [],
            selectedSwitches: []
        }
    }

    componentDidMount () {
        this.fetchCustomerCreditAccounts()

        const channelAccountRequest = PusherService.getInstance().subscribe('account-request')
        channelAccountRequest.bind('new', (data) => {
            this.setState({
                customerCreditAccounts: [ ...this.state.customerCreditAccounts, data ]
            })
        })
        channelAccountRequest.bind('remove', (data) => {
            const customerCreditAccounts = _.filter(this.state.customerCreditAccounts, obj => obj.id !== data.id)
            this.setState({ customerCreditAccounts })
        })
    }

    fetchCustomerCreditAccounts = () => {
        client.get(API_CUSTOMER_CREDIT_ACCOUNT_URL, { params: { status: 'pending' } }).then(res => {
            this.setState({ customerCreditAccounts: res.data })
        })
    }

    updateCustomerCreditAccount = (customerCreditAccountId, data) => {
        return client.put(API_CUSTOMER_CREDIT_ACCOUNT_DETAIL_URL.replace('customerCreditAccountId', customerCreditAccountId), data)
    }

    toggleApprovedSwitch = (row) => {
        this.setState({
            selectedSwitches: [
                row.id,
                ...this.state.selectedSwitches
            ]
        }, () => {
            Swal.fire({
                title: 'อนุมัติคำขอเปิดปัญชี',
                text: 'กรอกข้อมูลบัญชีที่สร้างให้ลูกค้า',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'ยืนยัน',
                cancelButtonText: 'ยกเลิก',
                input: 'text',
                html: `
                    <div class='form-group'>
                        <label for='username'>Username</label>
                        <input id='username' type='text' name='username' class='form-control'>
                    </div>
                    <div class='form-group'>
                        <label for='password'>Password</label>
                        <input id='password' type='text' name='password' class='form-control'>
                    </div>
                `,
                focusConfirm: false,
                inputValidator: (value) => {
                    let errorMessage = ''
                    const username = document.getElementById('username').value
                    const password = document.getElementById('password').value

                    if (!username) {
                        errorMessage = 'จำเป็นต้องระบุ Username'
                    }
                    if (!password) {
                        const message = (errorMessage.length > 0) ? `${errorMessage} และ ` : ''
                        errorMessage = `${message}จำเป็นต้องระบุ Password`
                    }
                    if (errorMessage.length > 0) return errorMessage
                },
                preConfirm: () => {
                    return {
                        username: document.getElementById('username').value,
                        password: document.getElementById('password').value
                    }
                },
                onBeforeOpen: () => {
                    Swal.getInput().style.display = 'none'
                }
            }).then((result) => {
                if (result.value) {
                    this.updateCustomerCreditAccount(row.id, {
                        status: STATUS_APPROVED,
                        username: result.value.username,
                        password: result.value.password
                    }).then(res => {
                        Swal.fire({
                            title: 'บันทึกข้อมูลสำเร็จ',
                            type: 'success',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                }
            })
        })
    }

    onClickRejectedButton = (row) => {
        Swal.fire({
            title: 'คุณต้องการที่จะยกเลิกรายการนี้หรือไม่',
            text: 'ข้อมูลนี้ไม่ถูกลบ แต่เป็นการยกเลิกรายการเท่านั้น',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
            if (result.value) {
                this.updateCustomerCreditAccount(row.id, { status: STATUS_REJECTED }).then(res => {
                    Swal.fire({
                        title: 'ยกเลิกรายการสำเร็จ',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
            }
        })
    }

    getTableBody () {
        const {
            customerCreditAccounts,
            selectedSwitches
        } = this.state
        if (customerCreditAccounts.length === 0) {
            return getNoDataTableBody(3)
        }
        return customerCreditAccounts.map(row => {
            const isChecked = selectedSwitches.indexOf(row.id) !== -1
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.customer.fullname}</TableCell>
                    <TableCell>{row.game.name}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch value={row.id} color='primary' onChange={e => this.toggleApprovedSwitch(row)} checked={isChecked} />
                        } label='อนุมัติ' className='mb-0' />
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.onClickRejectedButton(row)}>
                            <CancelIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>
                            ยกเลิกรายการ
                        </UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>คำขอเปิดบัญชีเครดิต</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>คำขอเปิดบัญชีเครดิต</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <h4 className='card-title'>รายการคำขอเปิดบัญชีเครดิต</h4>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ลูกค้า</TableCell>
                                    <TableCell>เกม</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>
        )
    }
}

export default withRouter(OpenAccountRequestView)
