import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import client from '../../utils/client'
import Swal from 'sweetalert2'
import { API_PROMOTION_URL } from '../../utils/urls'
import FormPromotion from '../../components/form-layouts/form-layout-promotion'

class PromotionCreateView extends Component {
    onFormSubmit = (value, reset) => {
        const {
            history
        } = this.props
        client.post(API_PROMOTION_URL, value).then(res => {
            reset()
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            history.replace({ pathname: `/promotions/${res.data.id}` })
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>เพิ่มโปรโมชั่น</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/promotions'>โปรโมชั่น</Link>
                        </li>
                        <li className='breadcrumb-item active'>เพิ่ม</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <FormPromotion onFormSubmit={this.onFormSubmit} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(PromotionCreateView)
