import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import client from '../../../utils/client'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import Swal from 'sweetalert2'
import Switch from '@material-ui/core/Switch/index'
import CancelIcon from '@material-ui/icons/Cancel'
import Table from '@material-ui/core/Table/index'
import TableHead from '@material-ui/core/TableHead/index'
import TableRow from '@material-ui/core/TableRow/index'
import TableCell from '@material-ui/core/TableCell/index'
import TableBody from '@material-ui/core/TableBody/index'
import Paper from '@material-ui/core/Paper/index'
import { getNoDataTableBody } from '../../../components/table'
import { UncontrolledTooltip } from 'reactstrap'
import { getBankIcon, thousandFormat } from '../../../utils/helpers'
import _ from 'lodash'
import FormControlLabel from '@material-ui/core/FormControlLabel/index'
import { API_SYSTEM_BANK_ACCOUNT_DETAIL_URL, API_SYSTEM_BANK_ACCOUNT_URL } from '../../../utils/urls'

class SystemBankAccountListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            systemBankAccounts: []
        }
    }

    componentDidMount () {
        this.fetchSystemAccounts()
    }

    fetchSystemAccounts = () => {
        client.get(API_SYSTEM_BANK_ACCOUNT_URL).then(res => {
            this.setState({
                systemBankAccounts: res.data
            })
        })
    }

    toggleIsActiveSwitch = (row) => {
        const isActive = !row.is_active
        client.put(API_SYSTEM_BANK_ACCOUNT_DETAIL_URL.replace('systemBankAccountId', row.id), { is_active: isActive }).then(res => {
            const systemBankAccounts = this.state.systemBankAccounts.map(obj => {
                if (obj.id === row.id) obj.is_active = isActive
                return obj
            })
            this.setState({ systemBankAccounts })
        })
    }

    removeSystemAccount = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_SYSTEM_BANK_ACCOUNT_DETAIL_URL.replace('systemBankAccountId', row.id)).then(res => {
                    const systemBankAccounts = _.filter(this.state.systemBankAccounts, obj => obj.id !== row.id)
                    this.setState({ systemBankAccounts }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    getTableBody = () => {
        const {
            systemBankAccounts
        } = this.state
        if (systemBankAccounts.length === 0) {
            return getNoDataTableBody(5)
        }
        return systemBankAccounts.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.account_no}</TableCell>
                    <TableCell>{getBankIcon(row.bank.code)} {row.bank.name}</TableCell>
                    <TableCell>{thousandFormat(row.balance)}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch color='primary' value={row.id} checked={row.is_active} onChange={e => this.toggleIsActiveSwitch(row)} />
                        } label='ใช้งาน' className='mb-0' />
                        <Link to={`/system-accounts/banks/${row.id}`} >
                            <button id={`btnEdit${row.id}`} className='btn btn-icon'><EditIcon /></button>
                        </Link>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removeSystemAccount(row)}><CancelIcon /></button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>ลบข้อมูล</UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>บัญชีระบบ</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts'>บัญชีระบบ</Link>
                        </li>
                        <li className='breadcrumb-item active'>ธนาคาร</li>
                    </ol>
                </div>

                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายชื่อบัญชีธนาคาร</h4>
                            <div className='ml-auto'>
                                <Link to='/system-accounts/banks/create' className='btn btn-info' >
                                    <AddIcon />เพิ่มบัญชีธนาคาร
                                </Link>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อบัญชี</TableCell>
                                    <TableCell>หมายเลขบัญชี</TableCell>
                                    <TableCell>ธนาคาร</TableCell>
                                    <TableCell>จำนวนเงิน</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>)
    }
}

export default withRouter(SystemBankAccountListView)
