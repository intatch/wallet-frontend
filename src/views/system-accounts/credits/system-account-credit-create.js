import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import client from '../../../utils/client'
import Swal from 'sweetalert2'
import SystemAccountCreditForm from '../../../components/form-layouts/form-layout-system-credit-account'
import { API_SYSTEM_CREDIT_ACCOUNT_URL } from '../../../utils/urls'

class SystemAccountCreditCreateView extends Component {
    onFormSubmit = (value, reset) => {
        const {
            history
        } = this.props
        client.post(API_SYSTEM_CREDIT_ACCOUNT_URL, value).then(res => {
            reset()
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            history.replace({ pathname: `/system-accounts/credits/${res.data.id}` })
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>เพิ่มบัญชีเครดิต</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts'>บัญชีระบบ</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts/credits'>เครดิต</Link>
                        </li>
                        <li className='breadcrumb-item active'>เพิ่มบัญชีเครดิต</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <h4 className='card-title'>เพิ่มบัญชีเครดิต</h4>
                            <SystemAccountCreditForm onFormSubmit={this.onFormSubmit} />
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

export default withRouter(SystemAccountCreditCreateView)
