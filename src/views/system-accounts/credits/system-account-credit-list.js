import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import client from '../../../utils/client'
import EditIcon from '@material-ui/icons/Edit'
import AddIcon from '@material-ui/icons/Add'
import Swal from 'sweetalert2'
import Switch from '@material-ui/core/Switch'
import CancelIcon from '@material-ui/icons/Cancel'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getNoDataTableBody } from '../../../components/table'
import { UncontrolledTooltip } from 'reactstrap'
import _ from 'lodash'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL, API_SYSTEM_CREDIT_ACCOUNT_URL } from '../../../utils/urls'
import { thousandFormat } from '../../../utils/helpers'

class SystemAccountCreditListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            systemCreditAccounts: []
        }
    }

    componentDidMount () {
        this.fetchSystemAccountCredits()
    }

    fetchSystemAccountCredits = () => {
        client.get(API_SYSTEM_CREDIT_ACCOUNT_URL).then(res => {
            this.setState({
                systemCreditAccounts: res.data
            })
        })
    }

    getTableBody = () => {
        const {
            systemCreditAccounts
        } = this.state
        if (systemCreditAccounts.length === 0) {
            return getNoDataTableBody(5)
        }
        return systemCreditAccounts.map(row => {
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.name}</TableCell>
                    <TableCell>{row.game.name}</TableCell>
                    <TableCell>{thousandFormat(row.balance)}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch color='primary' value={row.id} checked={row.is_active} onChange={e => this.toggleIsActiveSwitch(row)} />
                        } label='ใช้งาน' className='mb-0' />
                        <Link to={`/system-accounts/credits/${row.id}`} >
                            <button id={`btnEdit${row.id}`} className='btn btn-icon'><EditIcon /></button>
                        </Link>
                        <UncontrolledTooltip placement='auto' target={`btnEdit${row.id}`}>แก้ไขข้อมูล</UncontrolledTooltip>
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.removeSystemAccount(row)}><CancelIcon /></button>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>ลบข้อมูล</UncontrolledTooltip>
                    </TableCell>
                </TableRow>
            )
        })
    }

    toggleIsActiveSwitch = (row) => {
        const isActive = !row.is_active
        client.put(API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL.replace('systemCreditAccountId', row.id), { is_active: isActive }).then(res => {
            const systemCreditAccounts = this.state.systemCreditAccounts.map(obj => {
                if (obj.id === row.id) {
                    obj.is_active = isActive
                }
                return obj
            })
            this.setState({ systemCreditAccounts })
        })
    }

    removeSystemAccount = (row) => {
        Swal.fire({
            title: 'ยืนยันการลบข้อมูล',
            type: 'question',
            showCancelButton: true,
            showCloseButton: true
        }).then(result => {
            if (result.value) {
                client.delete(API_SYSTEM_CREDIT_ACCOUNT_DETAIL_URL.replace('systemCreditAccountId', row.id)).then(res => {
                    const systemCreditAccounts = _.filter(this.state.systemCreditAccounts, obj => obj.id !== row.id)
                    this.setState({ systemCreditAccounts }, () => {
                        Swal.fire({
                            type: 'success',
                            title: 'ลบข้อมูลสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    })
                })
            }
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>บัญชีระบบ</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/system-accounts'>บัญชีระบบ</Link>
                        </li>
                        <li className='breadcrumb-item active'>เครดิต</li>
                    </ol>
                </div>

                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <div className='d-flex align-items-center'>
                            <h4 className='card-title'>รายชื่อบัญชีเครดิต</h4>
                            <div className='ml-auto'>
                                <Link to='/system-accounts/credits/create' className='btn btn-info' >
                                    <AddIcon />เพิ่มบัญชีเครดิต
                                </Link>
                            </div>
                        </div>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อบัญชี</TableCell>
                                    <TableCell>เกม</TableCell>
                                    <TableCell>จำนวนเครดิต</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>
        )
    }
}

function mapStateToProps (state) {
    return { ...state }
}

export default withRouter(
    connect(mapStateToProps)(SystemAccountCreditListView))
