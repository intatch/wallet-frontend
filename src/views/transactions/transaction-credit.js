import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import _ from 'lodash'

import client from '../../utils/client'
import {
    API_SYSTEM_CREDIT_ACCOUNT_URL,
    API_TRANSACTION_CREDIT_DETAIL_URL,
    API_TRANSACTION_CREDIT_URL
} from '../../utils/urls'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Switch from '@material-ui/core/Switch'
import CancelIcon from '@material-ui/icons/Cancel'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getTransactionState, thousandFormat } from '../../utils/helpers'
import { UncontrolledTooltip } from 'reactstrap'
import Swal from 'sweetalert2'
import { getNoDataTableBody } from '../../components/table'
import PusherService from '../../utils/pusher'
import { STATE_WITHDRAW } from '../../utils'
import PageviewIcon from '@material-ui/icons/Pageview'
import ModalTransactionCreditDetail from '../../components/modals/modal-transaction-credit-detail'

class CreditListView extends Component {
    constructor (props) {
        super(props)
        this.state = {
            selectedSwitches: [],
            transactionCredits: [],
            isModalOpen: false,
            transactionCredit: null
        }
    }

    componentDidMount () {
        this.fetchTransactionCredits()

        let channelTransactionCredits = PusherService.getInstance().subscribe('transaction-credit')
        channelTransactionCredits.bind('new', (data) => {
            this.setState({
                transactionCredits: [ ...this.state.transactionCredits, data ]
            })
        })

        channelTransactionCredits.bind('update', (data) => {
            const transactionCredits = this.state.transactionCredits.map(value => value.id === data.id ? data : value)
            this.setState({ transactionCredits })
        })

        channelTransactionCredits.bind('remove', (data) => {
            const transactionCredits = _.filter(this.state.transactionCredits, obj => obj.id !== data.id)
            this.setState({ transactionCredits })
        })
    }

    fetchTransactionCredits = () => {
        client.get(API_TRANSACTION_CREDIT_URL).then(res => {
            this.setState({ transactionCredits: res.data })
        })
    }

    fetchTransactionCreditDetail = (transactionCreditId) => {
        client.get(API_TRANSACTION_CREDIT_DETAIL_URL.replace('transactionCreditId', transactionCreditId)).then(res => {
            this.setState({
                isModalOpen: true,
                transactionCredit: res.data
            })
        })
    }

    updateTransactionCreditStatus = (transactionCreditId, data) => {
        return client.post(API_TRANSACTION_CREDIT_DETAIL_URL.replace('transactionCreditId', transactionCreditId), data)
    }

    toggleModal = () => {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
            transactionCredit: null
        })
    }

    toggleApprovedSwitch = (row) => {
        const transactionCreditId = row.id
        this.setState({
            selectedSwitches: [
                transactionCreditId,
                ...this.state.selectedSwitches
            ]
        }, () => {
            client.get(API_SYSTEM_CREDIT_ACCOUNT_URL, { params: { is_active: true } }).then(res => {
                let swalOptions = {
                    title: 'เลือกบัญชีทำรายการ',
                    text: 'เลือกบัญชีที่ใช้สำหรับการทำรายการ',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'ยืนยัน',
                    cancelButtonText: 'ยกเลิก'
                }

                if (row.state === STATE_WITHDRAW) {
                    let inputOptions = {}
                    _.each(res.data, item => {
                        inputOptions[item.id] = item.name
                    })
                    swalOptions = {
                        ...swalOptions,
                        input: 'select',
                        inputOptions: inputOptions,
                        inputPlaceholder: '--- กรุณาเลือก ---',
                        inputValidator: (value) => {
                            if (!value) return 'จำเป็นต้องระบุ'
                        }
                    }
                } else {
                    const inputElements = `
                        <div class='form-group'>
                            <label for='bonus_credit' class=''>โบนัสเครดิต</label>
                            <input id='bonus_credit' type='number' name='bonus_credit' class='form-control' value='0'>
                        </div>
                        <div class='form-group'>
                            <label for='system_account_id' class=''>บัญชีเครดิต</label>
                            <select id='system_account_id' name='system_account_id' class='form-control custom-select'>
                                <option value=''>--- กรุณาเลือก ---</option>
                                ${res.data.map(item => `<option value=${item.id}>${item.name}</option>`)}
                            </select>
                        </div>
                    `

                    swalOptions = {
                        ...swalOptions,
                        input: 'text',
                        html: inputElements,
                        focusConfirm: false,
                        inputValidator: (value) => {
                            let errorMessage = ''
                            const bonusCredit = document.getElementById('bonus_credit').value
                            const systemAccountId = document.getElementById('system_account_id').value

                            if (!bonusCredit) {
                                errorMessage = 'จำเป็นต้องระบุโบนัสเครดิต'
                            } else if (isNaN(bonusCredit) && parseInt(bonusCredit) < 0) {
                                errorMessage = 'จำนวนเงินไม่ถูกต้อง'
                            }
                            if (!systemAccountId) {
                                const message = (errorMessage.length > 0) ? `${errorMessage} และ ` : ''
                                errorMessage = `${message}จำเป็นต้องระบุบัญชีเครดิต`
                            }
                            if (errorMessage.length > 0) return errorMessage
                        },
                        preConfirm: () => {
                            return {
                                bonusCredit: document.getElementById('bonus_credit').value,
                                systemAccountId: document.getElementById('system_account_id').value
                            }
                        },
                        onBeforeOpen: () => {
                            Swal.getInput().style.display = 'none'
                        }
                    }
                }

                Swal.fire(swalOptions).then((result) => {
                    if (result.value) {
                        let requestData = {
                            is_approved: true
                        }
                        if (row.state === STATE_WITHDRAW) {
                            requestData = {
                                ...requestData,
                                system_account_id: result.value
                            }
                        } else {
                            requestData = {
                                ...requestData,
                                bonus_credit: result.value.bonusCredit,
                                system_account_id: result.value.systemAccountId
                            }
                        }
                        this.updateTransactionCreditStatus(transactionCreditId, requestData).then(res => {
                            Swal.fire({
                                title: 'อนุมัติรายการสำเร็จ',
                                type: 'success',
                                showConfirmButton: false,
                                timer: 1500
                            })
                        })
                    }
                    this.setState({
                        selectedSwitches: this.state.selectedSwitches.filter(value => value !== transactionCreditId)
                    })
                })
            })
        })
    }

    onClickInProgressButton = (transactionCreditId) => {
        client.put(API_TRANSACTION_CREDIT_DETAIL_URL.replace('transactionCreditId', transactionCreditId), { action: 'updateProgress' })
    }

    onClickRejectedButton = (row) => {
        const transactionCreditId = row.id
        Swal.fire({
            title: 'คุณต้องการที่จะยกเลิกรายการนี้หรือไม่',
            text: 'ข้อมูลนี้ไม่ถูกลบ แต่เป็นการยกเลิกรายการเท่านั้น',
            type: 'warning',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            inputValidator: (value) => {
                if (!value) return 'จำเป็นต้องระบุ'
            }
        }).then((result) => {
            if (result.value) {
                this.updateTransactionCreditStatus(transactionCreditId, {
                    is_approved: false,
                    remark: result.value
                }).then(res => {
                    Swal.fire({
                        title: 'ยกเลิกรายการสำเร็จ',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1500
                    })
                })
            }
        })
    }

    getInProgressIcon = (row) => {
        if (row.user) {
            return (
                <span key={`btnInProgress${row.id}`}>
                    <button id={`btnInProgress${row.id}`} className='btn btn-icon-active'>
                        <VisibilityIcon />
                    </button>
                    <UncontrolledTooltip placement='auto' target={`btnInProgress${row.id}`}>
                        อยู่ระหว่างดำเนินการโดย {row.user.fullname}
                    </UncontrolledTooltip>
                </span>
            )
        } else {
            return (
                <span key={`btnInProgress${row.id}`}>
                    <button id={`btnInProgress${row.id}`} className='btn btn-icon' onClick={e => this.onClickInProgressButton(row.id)}>
                        <VisibilityOffIcon />
                    </button>
                    <UncontrolledTooltip placement='auto' target={`btnInProgress${row.id}`}>
                        ยังไม่ดำเนินการ
                    </UncontrolledTooltip>
                </span>
            )
        }
    }

    getTableBody = () => {
        const {
            transactionCredits,
            selectedSwitches
        } = this.state
        if (transactionCredits.length === 0) {
            return getNoDataTableBody(5)
        }

        return transactionCredits.map(row => {
            const isChecked = selectedSwitches.indexOf(row.id) !== -1
            return (
                <TableRow key={row.id}>
                    <TableCell>{row.customer}</TableCell>
                    <TableCell>{row.game.name}</TableCell>
                    <TableCell>{thousandFormat(row.credit)}</TableCell>
                    <TableCell>{getTransactionState(row.state)}</TableCell>
                    <TableCell className='text-right'>
                        <FormControlLabel control={
                            <Switch value={row.id} color='primary' onChange={e => this.toggleApprovedSwitch(row)} checked={isChecked} />
                        } label='อนุมัติ' className='mb-0' />
                        <button id={`btnRejected${row.id}`} className='btn btn-icon' onClick={e => this.onClickRejectedButton(row)}>
                            <CancelIcon />
                        </button>
                        <button id={`btnView${row.id}`} className='btn btn-icon' onClick={e => this.fetchTransactionCreditDetail(row.id)}>
                            <PageviewIcon />
                        </button>
                        <UncontrolledTooltip placement='auto' target={`btnView${row.id}`}>
                            รายละเอียดการทำธุรกรรม
                        </UncontrolledTooltip>
                        <UncontrolledTooltip placement='auto' target={`btnRejected${row.id}`}>
                            ยกเลิกรายการ
                        </UncontrolledTooltip>
                        {this.getInProgressIcon(row)}
                    </TableCell>
                </TableRow>
            )
        })
    }

    render () {
        const { transactionCredit, isModalOpen } = this.state
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>เครดิต</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>เครดิต</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <h4 className='card-title'>รายการธุรกรรม</h4>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ชื่อผู้ใช้</TableCell>
                                    <TableCell>เกม</TableCell>
                                    <TableCell>รวม</TableCell>
                                    <TableCell>สถานะ</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
                <ModalTransactionCreditDetail transactionCredit={transactionCredit} isModalOpen={isModalOpen} toggle={this.toggleModal} />
            </div>
        )
    }
}

export default withRouter(CreditListView)
