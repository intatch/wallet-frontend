import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

import client from '../../utils/client'
import { API_TRANSACTION_URL } from '../../utils/urls'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'
import { getTransactionState, getTransactionStatus, getDateFormat, thousandFormat } from '../../utils/helpers'
import { UncontrolledTooltip } from 'reactstrap'
import { getNoDataTableBody } from '../../components/table'
import PageviewIcon from '@material-ui/icons/Pageview'

class TransactionHistoryList extends Component {
    constructor (props) {
        super(props)
        this.state = {
            transactions: []
        }
    }

    componentDidMount () {
        this.fetchTransactions()
    }

    fetchTransactions = () => {
        client.get(API_TRANSACTION_URL).then(res => {
            this.setState({ transactions: res.data })
        })
    }

    getTableBody () {
        const {
            transactions
        } = this.state
        if (transactions.length === 0) {
            return getNoDataTableBody(7)
        }
        return transactions.map(row => (
            <TableRow key={row.id}>
                <TableCell>{row.customer && row.customer.fullname}</TableCell>
                <TableCell>{getTransactionState(row.state)}</TableCell>
                <TableCell>{getDateFormat(row.created_at)}</TableCell>
                <TableCell>{thousandFormat(row.amount)}</TableCell>
                <TableCell>{getTransactionStatus(row.status)}</TableCell>
                <TableCell>{row.user && row.user.fullname}</TableCell>
                <TableCell className='text-right'>
                    <Link id={`btnView${row.id}`} to={`/transactions/${row.id}`} className='btn btn-icon'>
                        <PageviewIcon />
                    </Link>
                    <UncontrolledTooltip placement='auto' target={`btnView${row.id}`}>
                        ดูข้อมูล
                    </UncontrolledTooltip>
                </TableCell>
            </TableRow>
        ))
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>ประวัติการทำธุรกรรม</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item active'>ประวัติการทำธุรกรรม</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <Paper className='p-4'>
                        <h4 className='card-title'>รายการประวัติการทำธุรกรรม</h4>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>ลูกค้า</TableCell>
                                    <TableCell>ประเภท</TableCell>
                                    <TableCell>วันที่/เวลาทำรายการ</TableCell>
                                    <TableCell>จำนวนเงิน</TableCell>
                                    <TableCell>สถานะ</TableCell>
                                    <TableCell>เจ้าหน้าที่</TableCell>
                                    <TableCell />
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.getTableBody()}
                            </TableBody>
                        </Table>
                    </Paper>
                </div>
            </div>
        )
    }
}

export default withRouter(TransactionHistoryList)
