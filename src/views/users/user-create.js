import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { API_USER_URL } from '../../utils/urls'
import client from '../../utils/client'
import FormUser from '../../components/form-layouts/form-layout-user'
import Swal from 'sweetalert2'

class UserCreateView extends Component {
    handleSubmitUser = (value, reset) => {
        const {
            history
        } = this.props
        client.post(API_USER_URL, value).then(res => {
            reset()
            Swal.fire({
                type: 'success',
                title: 'บันทึกข้อมูลสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            history.replace({ pathname: `/users/${res.data.id}` })
        }).catch(err => {
            if (err.response.data.username) {
                Swal.fire({
                    type: 'error',
                    title: 'username ซ้ำ กรุณาเปลี่ยน username',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
        })
    }

    render () {
        return (
            <div className='page-wrapper'>
                <div className='page-breadcrumb'>
                    <h1 className='page-title'>เพิ่มพนักงาน</h1>
                    <ol className='breadcrumb'>
                        <li className='breadcrumb-item'>
                            <Link to='/'>หน้าแรก</Link>
                        </li>
                        <li className='breadcrumb-item'>
                            <Link to='/users'>พนักงาน</Link>
                        </li>
                        <li className='breadcrumb-item active'>เพิ่มพนักงาน</li>
                    </ol>
                </div>
                <div className='container-fluid'>
                    <div className='card m-4'>
                        <div className='card-body'>
                            <h4 className='card-title'>เพิ่มพนักงาน</h4>
                            <FormUser handleSubmitUser={this.handleSubmitUser} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(UserCreateView)
