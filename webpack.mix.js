const mix = require('laravel-mix')
const path = require('path')

let webpack = require('webpack')

mix.setPublicPath('public')
    .sass('scss/style.scss', 'public/dist/css/app.css')
    .react('src/index.js', 'public/dist/js/app.js')
    .extract(['fontawesome'])
    .webpackConfig({
        resolve: {
            modules: [
                'node_modules',
                path.resolve(__dirname, 'src')
            ]
        },
        devServer: {
            contentBase: path.resolve(__dirname, 'public'),
            port: 8001
        },
        plugins: [
            new webpack.EnvironmentPlugin(['MIX_API_URL'])
        ]
    })
